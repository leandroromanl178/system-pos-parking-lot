const { ipcRenderer } = require('electron');
const { parse } = require('uuid');
const dt = require('datatables.net-dt')(window, $);

$(document).ready(() => {

    // Funcion de JQuery para filtrar solo por numeros
    (function($) {
        $.fn.inputFilter = function(inputFilter) {
            return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
                if (inputFilter(this.value)) {
                    this.oldValue = this.value;
                    this.oldSelectionStart = this.selectionStart;
                    this.oldSelectionEnd = this.selectionEnd;
                } else if (this.hasOwnProperty("oldValue")) {
                    this.value = this.oldValue;
                    this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
                } else {
                    this.value = "";
                }
            });
        };
    }(jQuery));

    // checkRolToUser();

    $('.datatable').DataTable();
    $('.datatable-reports').DataTable({
        "order": [[ 8, "desc" ]]
    });

    $('.mask-money').on('keyup', function(e){
        let arrayMoney = $(this).val().split('.');
        let moneyClean = '';

        $.each(arrayMoney,((a,e)=>{moneyClean+=e}));

        let money = addPoints(moneyClean.toString()).split(',')[0];
        $(this).val(money);
    })


    $(".only-numbers").inputFilter(function(value) {
        return /^\d*$/.test(value);
    });

    $('.phone_us').mask('(000) 000-0000');
    
    /**
     * Acciones para fomulario login
     */
    $('#form-login').submit((e) => {
        e.preventDefault();

        let arrayData = {
            'username': $('#username').val(),
            'password': $('#password').val()
        };

        let data = JSON.stringify(arrayData);

        sendCheckLogin(data).then(statusLogin => {
            statusLogin = JSON.parse(statusLogin);
            console.log(statusLogin);
            if (statusLogin.status == false) {
                setTimeout(() => { $('#centralModalSm').modal('hide'); }, 500);

                $('#password').val('');
                setTimeout(() => {
                    if(!$("#centralModalSm").is(':visible')) {
                        alert(statusLogin.message);
                    }
                }, 700);
            }

            if (statusLogin.status){
                localStorage.setItem('user', statusLogin.message);
                localStorage.setItem('statusSession', true);

                setTimeout(() => {
                    ipcRenderer.send('loginAction');
                }, 700);
            }
        });
    });

    function sendCheckLogin(data) {
        return new Promise((resolve, reject) => {
            $('#centralModalSm').modal('show');

            let statusLogin = ipcRenderer.sendSync('checkLogin', data);
            resolve(statusLogin);
        });
    }

    /**
     * logout
     */
    $('#logout').click(() => {
        localStorage.removeItem('user');
        localStorage.removeItem('statusSession');

        ipcRenderer.send('logout');
    });

    /**
     * Verificando tarifas creadas por automotor
     */
    $('.automotor-to-rate').change(() => {
        let data = {
            'automotorId': $('.automotor-to-rate option:selected').val()
        };

        let response = ipcRenderer.sendSync('consultRatesForAutomotor', JSON.stringify(data))

        if (response.status == 200) {
            let rates = JSON.parse(response.rates);
            
            if (rates.length > 0) {
                $.each(rates, (key, rate) => {
                    $.each($('.type-rate option'), (keyOption, option) => {
                        if($(option).val() == rate.type_rate){
                            $(option).hide('fast');
                        }else{
                            $(option).show('fast');
                        }
                    });
                });
            }else{
                $.each($('.type-rate option'), (keyOption, option) => {
                    $(option).show('fast');
                });
            }
        }
    });

    /**
     * Formulario registro de tarifas
     */
    $('#createRate').submit((e) => {
        e.preventDefault();

        $('#createRate').children('button').attr('disabled', true);

        let arrayData = {
            'automotor': $('.automotor-to-rate').val(),
            'typeRate': $('.type-rate').val(),
            'rateValue': $('.rate-value').val()
        };

        let data = JSON.stringify(arrayData);
        let response = ipcRenderer.sendSync('createRate', data);

        if (response.status == 201) {
            toastSuccess('Registro creado', response.msg, 2500);

            setTimeout(() => {
                $('#nav-tarifas').click();
            }, 2500);
        }else{
            toastError('Error al registrar', response.msg, 2500)
        }
    });

    /**
     * Accion para eliminar la tarifa
     */
    $('.delete-rate').click(() => {
        e.preventDefault();

        let data = { id: $(this).attr('rateId') };
        let response  = ipcRenderer.sendSync('deleteRate', JSON.stringify(data));
        
        if(response.status == 200){
            toastSuccess('Registro eliminado', response.msg, 2500);

            setTimeout(() => {
                $('#nav-tarifas').click();
            }, 2500);
        }else{
            toastError('Error al eliminar', response.msg, 2500);
        }
    });

    /**
     * Accion para editar la tarifa
     */
    $('.edit-rate').click(function() {
        let data = { id: $(this).attr('rateId') };
        ipcRenderer.send('editRate', JSON.stringify(data));
    });

    $('#updateRate').submit((e) => {
        e.preventDefault();

        $('#updateRate').children('button').attr('disabled', true);

        let arrayData = {
            'id': $('.rate-id').val(),
            'automotorId': $('.rate-type-automotor option:selected').val(),
            'typeRate': $('.type-rate option:selected').val(),
            'valueRate': $('.rate-value').val()
        };

        let response = ipcRenderer.sendSync('updateRate', JSON.stringify(arrayData));

        if (response.status == 200) {
            toastSuccess('Exitoso', response.msg, 2500);

            setTimeout(() => {
                $('#nav-tarifas').click();
            }, 2500);
        }else{
            toastError('Error al actualizar', response.msg, 2500);
        }
    });

    /**
     * Registro de nuevos automotores
     */
    $('#createAutomotor').submit((e) => {
        e.preventDefault();

        $('#createAutomotor').children('button').attr('disabled', true);

        let arrayData = {
            'automotorName': $('.automotorName').val(),
            'item': $('.fn-check-item').is(':checked') == true ? $('.item').val() : '',
            'statusItem': $('.fn-check-item').is(':checked')
        };

        let data = JSON.stringify(arrayData);
        let response = ipcRenderer.sendSync('createAutomotor', data);

        if (response.status == 201) {
            toastSuccess('Registro creado', response.msg, 2500);

            setTimeout(() => {
                $('#nav-automotores').click();
            }, 2500);
        }else if (response.status == 302) {
            toastError('Error al registrar', response.msg, 2500);
        }else{
            toastError('Error al registrar', response.msg, 2500);
        }
    });

    $('#updateAutomotor').submit((e) => {
        e.preventDefault();

        $('#updateAutomotor').children('button').attr('disabled', true);

        let arrayData = {
            'id': $('.automotor-id').val(),
            'automotorName': $('.automotorName').val(),
            'item': $('.fn-check-item').is(':checked') == true ? $('.item').val() : '',
            'statusItem': $('.fn-check-item').is(':checked')
        };

        let response = ipcRenderer.sendSync('updateAutomotor', JSON.stringify(arrayData));

        if (response.status == 200) {
            toastSuccess('Exitoso', response.msg, 2500);

            setTimeout(() => {
                $('#nav-automotores').click();
            }, 2500);
        }else{
            toastError('Error al actualizar', response.msg, 2500);
        }
    });

    /**
     * Accion para eliminar el automotor
     */
    $('.delete-automotor').click(function(e){
        e.preventDefault();

        let data = { autoId: $(this).attr('autId') };
        let response  = ipcRenderer.sendSync('deleteAutomotor', JSON.stringify(data));
        
        if(response.status == 200){
            toastSuccess('Registro eliminado', response.msg, 2500);

            setTimeout(() => {
                $('#nav-automotores').click();
            }, 2500);
        }else{
            toastError('Error al eliminar', response.msg, 2500);
        }
    });

    /**
     * Accion para editar el automotor
     */
    $('.edit-automotor').click(function(e){
        e.preventDefault();

        let data = { autoId: $(this).attr('autId') };
        ipcRenderer.send('editAutomotor', JSON.stringify(data));
    });

    /**
     * Registro de entrada
     */
    $('#entry-register').submit((e) => {
        e.preventDefault();
        
        $('#entry-register').children('button').attr('disabled', true);

        let userData = localStorage.getItem('user');
        let startDateTime = moment(new Date($('.date-time-entry').val())).format('YYYY-MM-DDTHH:mm:ss.SSS');
        
        let arrayData = {
            'automotor': $('.automotor-load-rates').val(),
            'plate': $('.plate').val().toUpperCase(),
            'rate': $('.rates-by-automotor').val(),
            'startDateTime': startDateTime+'Z',
            'cantItems': $('.cant-item').val(),
            'userData': userData
        };

        let response = ipcRenderer.sendSync('registerEntry', JSON.stringify(arrayData));

        if (response.status == 201) {
            toastSuccess('Ingreso', response.msg, 2500);
            setTimeout(() => { $('.return-to-index').click(); }, 2500);
        }else{
            toastError('Error al registrar', response.msg, 2500)
        }
    });

    $('.inx-plate').change(function(){
        let plateArray = $(this).val().split('');
        let identMotorcycle = false;

        if (isNaN(parseInt(plateArray[5])) == true) {
            identMotorcycle = true;
        }

        $('.inx-automotor option:selected').removeAttr('selected', true)

        $.each($('.inx-automotor option'), (key, option) => {
            let automotorName = $(option).text().toUpperCase();
            
            if(identMotorcycle && automotorName == 'MOTO'){
                $(option).attr('selected', true);
            }

            if(!identMotorcycle && automotorName == 'CARRO'){
                $(option).attr('selected', true);
            }
        });

        $.each($('.rates-by-automotor option'), (key, option) => {
            $(option).remove();
        });

        initAddRates($('.inx-automotor option:selected')).then((r) => {
            if (r){
                $.each($('.rates-by-automotor option'), (key, options) => {
                    if(key == 0){
                        $(options).attr('selected', true);
                    }
                });
            }
        });
    });

    function initAddRates(option){
        return new Promise((resolve, reject) => {
            let selected = $(option).val();
            let arrayData = {'automotorId': selected}
            let data = ipcRenderer.sendSync('consultRatesForAutomotor', JSON.stringify(arrayData));

            if (data.status == 200) {
                let rates = JSON.parse(data.rates);
                let automotor = JSON.parse(data.automotor);
                let name = '';

                if (automotor.enabled_items == true) {
                    $('.div-content-form-items').show('slow');
                    $('.item').val(automotor.automotor_items);
                    $('.cant-item').attr('required', true);
                }else{
                    $('.div-content-form-items').hide('slow');
                    $('.item').val('');
                    $('.cant-item').removeAttr('required', true);
                }

                if (rates.length === 0) {
                    alert('No hay tarifas registradas para este automotor');
                    return resolve(false);
                }

                $.each(rates, (key, rate) => {
                    switch (rate.type_rate) {
                        case 1: name = 'Hora ('+ rate.value_rate + ')'; break;
                        case 2: name = 'Diario ('+ rate.value_rate + ')'; break;
                        case 3: name = 'Semanal ('+ rate.value_rate + ')'; break;
                        case 4: name = 'Mensual ('+ rate.value_rate + ')'; break;
                    }

                    $('.rates-by-automotor').append(new Option(name, rate.id));
                });

                resolve(true);
            }
        });
    }

    /**
     * Acciones para el navbar
     */
    $('#nav-tarifas').click(() => {
        ipcRenderer.send('ratesList');
    });

    $('#nav-automotores').click(() => {
        ipcRenderer.send('automotorList');
    });
    
    $('#nav-usuarios').click(() => {
        ipcRenderer.send('usersList');
    });

    $('#nav-informes').click(() => {
        $('#centralModalSm').modal('show');
        ipcRenderer.send('reports');
    });

    $('#nav-box').click(() => {
        ipcRenderer.send('box');
    });

    $('#nav-support').click(() => {
        ipcRenderer.send('supportIndex');
    });
    

    $('.return-to-index').click(() => {
        ipcRenderer.send('returnToIndex');
    });

    $('.newRate').click(() => {
        ipcRenderer.send('newRate');
    });

    $('.newAutomotor').click(() => {
        ipcRenderer.send('newAutomotor');
    });

    $('.fn-check-item').change(() => {
        let statusCheck = $('.fn-check-item').is(':checked');
        statusCheck ? $('.item').removeClass('hidden') : $('.item').addClass('hidden');
    });

    $('.automotor-load-rates').change(() => {
        let automotorId = $('.automotor-load-rates option:selected').val();
        let arrayData = {
            'automotorId': automotorId
        }

        let data = ipcRenderer.sendSync('consultRatesForAutomotor', JSON.stringify(arrayData));

        if (data.status == 200) {
            let rates = JSON.parse(data.rates);
            let automotor = JSON.parse(data.automotor);
            let name = '';

            $.each($('.rates-by-automotor option'), (key, options) => {
                if ($(options).val() != '') {
                    $(options).remove();
                }
            });

            if (automotor.enabled_items == true) {
                $('.div-content-form-items').show('slow');
                $('.item').val(automotor.automotor_items);
                $('.cant-item').attr('required', true);
            }else{
                $('.div-content-form-items').hide('slow');
                $('.item').val('');
                $('.cant-item').removeAttr('required', true);
            }

            if (rates.length === 0) {
                alert('No hay tarifas registradas para este automotor');
                return;
            }

            $.each(rates, (key, rate) => {
                switch (rate.type_rate) {
                    case 1: name = 'Hora ('+ rate.value_rate + ')'; break;
                    case 2: name = 'Diario ('+ rate.value_rate + ')'; break;
                    case 3: name = 'Semanal ('+ rate.value_rate + ')'; break;
                    case 4: name = 'Mensual ('+ rate.value_rate + ')'; break;
                }

                $('.rates-by-automotor').append(new Option(name, rate.id));
            });
        }
    });

    $('body').on('click', '.select-row', function() {
        $.each($('.select-row'), (key, row) => {
            $(row).removeAttr('style', true);
        });

        let recordId = $(this).attr('recordid');
        let arrayData = {
            'recordId': recordId
        };

        ipcRenderer.send('checkRecordInList', JSON.stringify(arrayData));
    });

    $('.record-to-exit').click(function(){
        let user = JSON.parse(localStorage.getItem('user'));

        let totalAmount = $('.total-amount').val();
        let amountReceived = $('.amount-received').val();

        let totalAmountArray = totalAmount.split('.');
        let amountReceivedArray = amountReceived.split('.');
        let totalAmountClean = '';
        let amountReceivedClean = '';

        $.each(totalAmountArray, (key, number) => { totalAmountClean += number });
        $.each(amountReceivedArray, (key, number) => { amountReceivedClean += number });

        if (Number(totalAmountClean) > Number(amountReceivedClean)) {
            toastErrorFancy('Error', 'Hace falta dinero para completar', 2500);
            return false;
        }

        let data = {
            'recordId': $(this).attr('recordId'),
            'user_id': user.user_id,
            'amount': totalAmount,
            'amountReceived': amountReceived
        };

        ipcRenderer.send('recordToExit', JSON.stringify(data));
    });

    $('.add-user').click(() => {
        ipcRenderer.send('newUser');
    });

    /**
     * Verificando la repeticion del username
     */
    $('.username').change(function(){
        let data = { 'username': $(this).val() };
        let response = ipcRenderer.sendSync('checkUsername', JSON.stringify(data));
        console.log(response);
        if (response.status == true) {
            toastError('Nombre de usuario invalido', response.msg, 2500);
            $(this).val('');
        }else{
            console.log(response.msg);
        }
    });

    /**
     * Validacion de contraseñas
     */
    $('.password, .confPassword').change(() => {
        let password = $('.password').val();
        let confPassword = $('.confPassword').val();

        if (password != '' && confPassword != '') {
            if (password !== confPassword) {
                toastError('Error validacion de contraseñas', 'Las contraseñas no coinciden', 2500);
                $('.confPassword').val('');
            }
        }
    });

    /**
     * Creacion de usuario
     */
    $('#createUser').submit((e) => {
        e.preventDefault();

        $('#createUser').children('button').attr('disabled', true);

        const data = {
            'name': $('.name').val(),
            'lastname': $('.lastname').val(),
            'rol': $('.rol').val(),
            'username': $('.username').val(),
            'password': $('.password').val()
        };

        const response = ipcRenderer.sendSync('createUser', JSON.stringify(data));

        if (response.status == 201) {
            toastSuccess('Usuario creado', response.msg, 2500);
            setTimeout(() => { $('#nav-usuarios').click(); }, 2500)
        }else{
            toastError('Error', response.msg, 2500);
        }

    });

    /**
     * Eliminando usuario
     */
    $('.delete-user').click(function(e){
        e.preventDefault();

        const localUser = JSON.parse(localStorage.getItem('user'));

        if (localUser.isAdmin == true) {
            const data = { 'id': $(this).attr('userId') };
            const response = ipcRenderer.sendSync('deleteUser', JSON.stringify(data));

            if (response.status == 200) {
                toastSuccess('Usuario eliminado', response.msg, 2500);
                setTimeout(() => { $('#nav-usuarios').click(); }, 2500)
            }else{
                toastError('Error', response.msg, 2500);
            }
        }else{
            toastError('Permiso denegado', 'Necesita permisos de administrador', 2500);
        }
    });

    /**
     * Accion para entrar a editar el usuario
     */
    $('.edit-user').click(function(e){
        e.preventDefault();

        const localUser = JSON.parse(localStorage.getItem('user'));

        if (localUser.isAdmin == true) {
            const data = { 'id': $(this).attr('userId') };
            ipcRenderer.send('editUser', JSON.stringify(data));
        }else{
            toastError('Permiso denegado', 'Necesita permisos de administrador', 2500);
        }
    });

    $('#updateUser').submit((e) => {
        e.preventDefault();

        $('#updateUser').children('button').attr('disabled', true);

        const data = {
            'id': $('.userId').val(),
            'name': $('.name').val(),
            'lastname': $('.lastname').val(),
            'rol': $('.rol').val(),
            'username': $('.username').val(),
            'password': $('.password').val()
        };

        const response = ipcRenderer.sendSync('updateUser', JSON.stringify(data));

        if (response.status == 200) {
            toastSuccess('Usuario actualizado', response.msg, 2500);
            setTimeout(() => { $('#nav-usuarios').click(); }, 2500)
        }else{
            toastError('Error', response.msg, 2500);
        }
    });

    $('.date-entry').change(function() { 
        $('.date-payment').val(moment(new Date($(this).val())).add(30, 'days').format('YYYY-MM-DD'));
    });

    $('.new-report').submit(function(e){
        e.preventDefault();

        $('.new-report').children('button').attr('disabled', true);

        let data = {
            'automotor': $('.report-automotor option:selected').val(),
            'plate': $('.plate').val().toUpperCase(),
            'rate': $('.rate').val(),
            'date_entry': $('.date-entry').val(),
            'date_payment': $('.date-payment').val(),
            'client_full_name': $('.client-full-name').val(),
            'client_id': $('.client-full-name').attr('clientId'),
            'client_document': $('.client-document').val(),
            'client_phone': $('.client-phone').val()
        };
        
        const response = ipcRenderer.sendSync('newReport', JSON.stringify(data));
        
        if (response.status == 201) {
            toastSuccess('Nuevo informe', response.msg, 2500);
            setTimeout(() => { $('#nav-informes').click(); }, 2500)
        }else{
            toastError('Error', response.msg, 2500);
        }
    });

    $('.client-full-name').keyup(function () {
        let term = $(this).val();

        $(this).attr('clientId', '');
        $('.results-clients ul li').remove();

        if (term != '') {
            let response = ipcRenderer.sendSync('searchClient', JSON.stringify({'term': term}));

            if(response.status == 200){
                let clients = response.clients;
                
                $.each(clients, (key, client) => {
                    let data = client.dataValues;
                    $('.results-clients ul').append('<li class="list-group-item fn-select-client style-select-client" clientId="'+ data.id +'" clientDoc ="'+ data.document +'" clientPhone="'+ data.phone +'">'+ data.full_name +' ('+ data.document +')</li>');
                });
            }
        }
    });

    $('body').on('click', '.fn-select-client', function(){
        const id = $(this).attr('clientId');
        const document = $(this).attr('clientDoc');
        const name = $(this).text().split('(');
        const phone = $(this).attr('clientPhone');

        $('.client-full-name').val(name[0]).attr('clientId', id);
        $('.client-document').val(document);
        $('.client-phone').val(phone);

        $('.results-clients ul li').remove();
    });

    $('.finalize-monthly-payment').click((e) => {
        e.preventDefault();

        let paymentMonth = $('.report-month-rate').val();
        let paymentReceived = $('.report-payment').val();

        let paymentMonthArray = paymentMonth.split('.');
        let paymentReceivedArray = paymentReceived.split('.');
        let paymentMonthClean = '';
        let paymentReceivedClean = '';

        $.each(paymentMonthArray, (key, number) => { paymentMonthClean += number });
        $.each(paymentReceivedArray, (key, number) => { paymentReceivedClean += number });

        if (Number(paymentMonthClean) > Number(paymentReceivedClean)) {
            toastErrorFancy('Error', 'Hace falta dinero para completar la mensualidad', 2500);
            return false;
        }
        
        let user = JSON.parse(localStorage.getItem('user'));
        
        const data = {
            'paymentMonth': paymentMonth,
            'paymentReceived': paymentReceived,
            'id': $('.report-id').val(),
            'userId': user.user_id,
            'enabledNextCharge': $('#enable-next-charge').is(':checked')
        };
        
        const response = ipcRenderer.sendSync('finalizeMonthly', JSON.stringify(data));

        if(response.status == 200){
            $('#exit-report').modal('hide');
            $('#nav-informes').click();
        }
    });

    let reportId = null;

    $('body').on('click', '.select-row-report', function (){
        reportId = $(this).attr('reportId');
    });

    $('body').on('click', '.report-exit', function() {
        
        $('.report-id').val('');
        $('.report-month-rate').val('');
        $('.report-payment').val('');

        let data = ipcRenderer.sendSync('reportExit', JSON.stringify({'reportId': reportId}));
        data = JSON.parse(data);

        $('.report-id').val(data.report.id);
        $('.report-month-rate').val(data.report.rate);
        
        $('#exit-report').modal('show');
        
        
    });

    $('body').on('click', '.report-edit', function(){

        let data = ipcRenderer.sendSync('reportEdit', JSON.stringify({'reportId': reportId}));

        if (data.status == 501) {
            toastError('Error', 'El registro ya no esta disponible', 2500);
            $('#nav-informes').click();
        }

        if(data.status == 200){
            let report = data.report.dataValues;

            $('.edit-report-id').val(report.id);
            $('.edit-report-start-date').val(moment(new Date(report.report_start_date)).add(1,'days').format('YYYY-MM-DD'));
            $('.edit-report-end-date').val(moment(new Date(report.report_end_date)).add(1,'days').format('YYYY-MM-DD'));

            $('#edit-report').modal('show');
        }
    });

    $('body').on('click', '#update-report', function(e){
        e.preventDefault();
        
        const data = {
            'newStartDate': $('.edit-report-start-date').val(),
            'newEndDate': $('.edit-report-end-date').val(),
            'reportId': $('.edit-report-id').val(),
        };

        let response = ipcRenderer.sendSync('reportUpdate', JSON.stringify(data));

        if (response.status == 200) {
            $('#edit-report').modal('hide');

            toastSuccess('Actualización', response.msg, 1500);
            setTimeout(() => { $('#nav-informes').click(); }, 1500)
        }else{
            toastError('Error', response.msg, 2500);
        }
    });

    $('body').on('click', '.report-delete', function(){
        let response = ipcRenderer.sendSync('reportDelete', JSON.stringify({'reportId': reportId}));

        if (response.status == 200) {
            toastSuccess('Alerta', response.msg, 1500);
            setTimeout(() => { $('#nav-informes').click(); }, 1500)
        }else{
            toastError('Error', response.msg, 2500);
        }
    });

    $('body').on('click', '.report-renove', function () {
        let data = ipcRenderer.sendSync('reportEdit', JSON.stringify({'reportId': reportId, 'renove': true}));

        if(data.status == 200){
            let report = data.report.dataValues;

            $('.renove-report-id').val('');
            $('.renove-report-rate').val('');
            $('.renove-report-start-date').val('');
            $('.renove-report-end-date').val('');

            $('.renove-report-id').val(report.id);
            $('.renove-report-rate').val(report.rate);
            $('.renove-report-start-date').val(moment(new Date()).format('YYYY-MM-DD'));
            $('.renove-report-end-date').val(moment(new Date($('.renove-report-start-date').val())).add(30, 'days').format('YYYY-MM-DD'));

            $('#renove-report').modal('show');
        }
    });

    $('.renove-report-start-date').change(function(){
        $('.renove-report-end-date').val(moment(new Date($(this).val())).add(30, 'days').format('YYYY-MM-DD'));
    });

    $('body').on('click', '#btn-renove-report', function(){
        const data = {
            'renoveStartDate': $('.renove-report-start-date').val(),
            'renoveEndDate': $('.renove-report-end-date').val(),
            'renoveNewRate': $('.renove-report-rate').val(),
            'id': $('.renove-report-id').val()
        };

        let response = ipcRenderer.sendSync('renoveReport', JSON.stringify(data));

        if (response.status == 200) {
            toastSuccess('Mensualidad', response.msg, 2500);
            setTimeout(() => { $('#nav-informes').click(); }, 2500)
        }else {
            toastError('Error', response.msg, 2500);
        }
    });

    $("#exit-report").on('shown.bs.modal', function(){
        $(this).find('.report-payment').focus();
    });

    $("#edit-report").on('shown.bs.modal', function(){
        $(this).find('.edit-report-start-date, .edit-report-end-date').focus();
    });

    $("#renove-report").on('shown.bs.modal', function(){
        $(this).find('.renove-report-start-date, .renove-report-end-date, .renove-report-rate').focus();
    });

    $('body').on('click', '#enable-next-charge', function(){
        !$(this).is(':checked') ? $('.next-charge').hide('slow') : $('.next-charge').show('slow');
    });

    $('#backup').click(() => {
        $('#confirmAccount').modal('show');
    });

    $('#confirm-account-admin').submit((e) => {
        e.preventDefault();

        const username = $('#username-confirm').val();
        const password = $('#password-confirm').val();
        
        const data = {
            'username': username,
            'password': password
        };

        const response = ipcRenderer.sendSync('confirmAccount', JSON.stringify(data));

        $('#username-confirm').val('');
        $('#password-confirm').val('');

        if (response.status != 200) {
            $('#confirmAccount').modal('hide');

            setTimeout(() => {
                toastError('Error', response.message, 2500);
            }, 300);

            return false;
        }

        const responseBackup = ipcRenderer.sendSync('chooseDirectoryBackup');
        $('#confirmAccount').modal('hide');

        if (responseBackup.status != 200) {
            setTimeout(() => {
                toastError('Error', responseBackup.message, 2500);
            }, 300);
        }else{
            setTimeout(() => {
                toastSuccess('Creacion de copia', responseBackup.message, 3500);
            }, 300);
        }
    });

    $('#mount-backup-copy').click(() => {
        const responseLoadBackup = ipcRenderer.sendSync('selectFileBackup');

        if (responseLoadBackup.status == 500 || responseLoadBackup.status == 400) {
            toastError('Error', responseLoadBackup.message, 2500);
        }else if (responseLoadBackup.status == 200){
            toastSuccess('Backup', responseLoadBackup.message, 3500);
        }else if (responseLoadBackup.status == 201){
            let file = responseLoadBackup.file;

            $('#modal-backup').modal('show');
            
            $('.setTextResponseBackUp').append(responseLoadBackup.message);
            $('.mounthFileBackup').attr('fileBackup', file);
        }  
    });

    $('.mounthFileBackup').click(function (){
        $('#modal-backup').modal('hide');
        $('#centralModalSm').modal('show');
        
        $('#centralModalSm p').text('Cargando Datos, espere por favor ...');

        let file = $(this).attr('fileBackup');
        const data = { file: file }

        setTimeout(() => {
            const pushBackup = ipcRenderer.sendSync('pushBackUp', JSON.stringify(data));
        
            if (pushBackup.status == 200) {
                toastSuccess('Backup', pushBackup.message, 3500);
            }

            $('#centralModalSm').modal('hide');
        }, 1000);
        
    });

    function toastSuccess(textHeader, textBody, timeOnClose) {
        let toast = '<div class="toast-success toast" style="position: absolute; right: 0; z-index: 1;" data-autohide="false">'+
            '<div class="toast-header" style="background-color: #4285f4; color: black;">'+
                '<small class="mr-auto">'+ textHeader +'</small>'+
                // '{# <small>11 mins ago</small> #}'+
                '<button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">'+
                    '<span aria-hidden="true">&times;</span>'+
                '</button>'+
            '</div>'+
            '<div class="toast-body">'+ textBody +'</div>'+
        '</div>';

        $('.body-to-nav').append(toast);
        $('.toast-success').toast('show');

        setTimeout(() => {
            $('.toast-success').toast('hide').remove();
        }, timeOnClose);
    }

    function toastError(textHeader, textBody, timeOnClose) {
        let toast = '<div class="toast-error toast" style="position: absolute; right: 0; z-index: 1;" data-autohide="false">'+
            '<div class="toast-header" style="background-color: #dc3545; color: black;">'+
                '<strong class="mr-auto">'+ textHeader +'</strong>'+
                // '{# <small>11 mins ago</small> #}'+
                '<button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">'+
                    '<span aria-hidden="true">&times;</span>'+
                '</button>'+
            '</div>'+
            '<div class="toast-body">'+ textBody +'</div>'+
        '</div>';

        $('.body-to-nav').append(toast);
        $('.toast-error').toast('show');

        setTimeout(() => {
            $('.toast-error').toast('hide').remove();
        }, timeOnClose);
    }

    function toastErrorFancy(textHeader, textBody, timeOnClose) {
        let toast = '<div class="toast-error toast" style="position: absolute; right: 0; z-index: 1;" data-autohide="false">'+
            '<div class="toast-header" style="background-color: #dc3545; color: black;">'+
                '<strong class="mr-auto">'+ textHeader +'</strong>'+
                // '{# <small>11 mins ago</small> #}'+
                '<button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">'+
                    '<span aria-hidden="true">&times;</span>'+
                '</button>'+
            '</div>'+
            '<div class="toast-body">'+ textBody +'</div>'+
        '</div>';

        $('body').prepend(toast);
        $('.toast-error').toast('show');

        setTimeout(() => {
            $('.toast-error').toast('hide').remove();
        }, timeOnClose);
    }

    function addPoints(nStr) {
        nStr += '';
        var x = nStr.split('.');
        var x1 = x[0];
        var x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
    
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        
        var result = x1 + x2;
        return result + ',00';
    }

    /**
     * Funcion para verificar los roles y quitar determinados permisos de acceso
     */
    // function checkRolToUser(){
    //     let user = JSON.parse(localStorage.getItem('user'));
        
    //     if (user != null) {
    //         if (!user.isAdmin) {
    //             $('#nav-usuarios').closest('li').hide();
    //             $('#nav-box').closest('li').hide();
    //         }else{
    //             $('#nav-usuarios').closest('li').show();
    //             $('#nav-box').closest('li').show();
    //         }
    //     }
    // }



});