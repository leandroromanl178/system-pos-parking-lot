const { Sequelize } = require('sequelize');
const db = {};

const sequelize = new Sequelize('parkinglotdb', 'root', '', {
    host: 'localhost',
    dialect: 'mysql'
});

(async () => {
    try {
        await sequelize.authenticate();
        console.log('Connection has been established successfully.');
    } catch (error) {
        console.error('Unable to connect to the database:', error);
    }
})


db.sequelize = sequelize;
db.Sequelize = Sequelize;
db.queryTypes = Sequelize.queryTypes;

module.exports = db;