'use strict';

const fs = require('fs');
const path = require('path');
const { app } = require('electron');
const {Sequelize} = require('sequelize');
const basename = path.basename(__filename);
const env = process.env.NODE_ENV || 'development';
const config = require(__dirname + '/../config/config.json')[env];
const db = {};

let dbFile = '';
if(env == 'production'){
  dbFile = path.join(__dirname, '../../../' + config['storage']);
}else{
  dbFile = config['storage'];
}

let sequelize = new Sequelize({ dialect: config['dialect'], storage: dbFile });

fs
  .readdirSync(__dirname)
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(file => {
    const model = require(path.join(__dirname, file))(sequelize, Sequelize.DataTypes);
    db[model.name] = model;
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;
db.routedb = dbFile;

module.exports = db;
