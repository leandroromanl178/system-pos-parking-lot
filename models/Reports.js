'use strict';
const { Model, DataTypes } = require('sequelize');

module.exports = (sequelize) => {
    class Reports extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    };

    Reports.init({
        id: {
            allowNull: false,
            primaryKey: true,
            type: DataTypes.INTEGER,
            autoIncrement: true
        },
        client: {
            type: DataTypes.UUID,
            allowNull: false,
            references: {
                model: 'Clients',
                key: 'id'
            }
        },
        automotor: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: 'Automotores',
                key: 'id'
            }
        },
        plate: {
            type: DataTypes.STRING,
            allowNull: false
        },
        rate: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        report_start_date: {
            type: DataTypes.DATE,
            allowNull: false
        },
        report_end_date: {
            type: DataTypes.DATE,
            allowNull: false
        },
        status: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        },
        delete_at: {
            type: DataTypes.DATE,
            allowNull: true
        }
    }, {
        sequelize,
        modelName: 'Reports',
        timestamps: false
    });

    return Reports;
};