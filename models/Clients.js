'use strict';
const { Model, DataTypes } = require('sequelize');

module.exports = (sequelize) => {
    class Clients extends Model {
        /**
         * Helper method for defining associations.
         * This method is not a part of Sequelize lifecycle.
         * The `models/index` file will call this method automatically.
         */
        static associate(models) {
            // define association here
        }
    };

    Clients.init({
        id: {
            allowNull: false,
            primaryKey: true,
            type: DataTypes.UUID
        },
        full_name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        document: {
            type: DataTypes.STRING,
            allowNull: false
        },
        phone: {
            type: DataTypes.STRING,
            allowNull: false
        }
    }, {
        sequelize,
        modelName: 'Clients'
    });

    return Clients;
};