'use strict';
const { Model, DataTypes } = require('sequelize');

module.exports = (sequelize) => {
  class Rates extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };

Rates.init({
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.INTEGER,
      autoIncrement: true
    },
    automotor_type: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'Automotores',
        key: 'id'
      }
    },
    type_rate: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    value_rate: {
      type: DataTypes.STRING,
      allowNull: false
    }
}, {
    sequelize,
    modelName: 'Rates',
    timestamps: false
});

  return Rates;
};