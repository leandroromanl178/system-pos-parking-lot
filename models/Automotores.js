'use strict';
const { Model, DataTypes } = require('sequelize');

module.exports = (sequelize) => {
  class Automotores extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };

Automotores.init({
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.INTEGER,
      autoIncrement: true
    },
    automotor_type: {
      type: DataTypes.STRING,
      allowNull: false
    },
    automotor_items: {
      type: DataTypes.STRING,
      allowNull: true
    },
    enabled_items: {
      type: DataTypes.BOOLEAN,
      allowNull:false
    }
}, {
    sequelize,
    modelName: 'Automotores',
    timestamps: false
});

  return Automotores;
};