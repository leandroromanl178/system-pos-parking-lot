'use strict';
const { Model, DataTypes } = require('sequelize');

module.exports = (sequelize) => {
    class Records extends Model {};

    Records.init({
        id: {
            allowNull: false,
            primaryKey: true,
            type: DataTypes.UUID
        },
        user_employed: {
            type: DataTypes.UUID,
            allowNull: false,
            references: {
                model: 'Users',
                key: 'user_id'
            }
        },
        automotor: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: 'Automotores',
                key: 'id'
            }
        },
        cant_automotor_items: {
            type: DataTypes.INTEGER,
            allowNull: true
        },
        rate: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
                model: 'Rates',
                key: 'id'
            }
        },
        plate: {
            type: DataTypes.STRING,
            allowNull: false
        },
        start_date_time: {
            type: DataTypes.DATE,
            allowNull: false
        },
        end_date_time: {
            type: DataTypes.DATE,
            allowNull: true
        },
        delete_at: {
            type: DataTypes.DATE,
            allowNull: true
        }
    }, {
        sequelize,
        modelName: 'Records',
        timestamps: false
    });

    return Records;
};