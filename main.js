/**
 * Imports
 */
const { app, BrowserWindow, ipcMain, Main, dialog } = require('electron');
const { PosPrinter } = require('electron-pos-printer');
const path = require('path');
const url = require('url');
const bcrypt = require('bcrypt');
const twig = require('electron-twig');
const { v4: uuidv4 } = require('uuid');
const { QueryTypes, json } = require('sequelize');
const fs = require('fs');
const moment = require('moment');

/**
 * Connection
 */
const db = require('./models/index');
const { Op } = require('sequelize');

/**
 * Models
 */
const Users = require('./models/Users')(db.sequelize);
const Rates = require('./models/Rates')(db.sequelize);
const Automotor = require('./models/Automotores')(db.sequelize);
const Record = require('./models/Records')(db.sequelize);
const Client = require('./models/Clients')(db.sequelize);
const Report = require('./models/Reports')(db.sequelize);
const Box = require('./models/Information')(db.sequelize);

let dataUser = {};
let win;
let winInfoToRecord;

/**
 * Descripcion en general para los tiquetes
 */
let ticketDescription = 'Conserve este recibo, el vehiculo sera entregado al portador, '+
        'En caso de perdida por favor, presentar la tarjeta de propiedad, NO aceptamos ordenes telefonicas ni escritas '+
        'Mientras el vehiculo este dentro de este parqueadero, el usuario respondera por daños que cause a terceros o al parqueadero '+
        'El parqueadero no es responsable por los daños al vehiculo por causa de incendio, sismo, hurto, u otros eventos '+
        'No se responde por objetos dejados dentro o fuera del vehiculo ';

/**
 * Carga el inicio de la ventana principal
 */
app.on('ready', async () => {
    
    win = new BrowserWindow({
        icon: './build/icon.ico',
        webPreferences: { nodeIntegration: true, contextIsolation: false }
    });

    win.maximize();
    win.removeMenu();
    // win.webContents.openDevTools();

    return loadTemplates('src/templates/login.html.twig', {
        'login': true
    });
});

/**
 * Verificando datos para cargar inicio de session
 */
ipcMain.on('checkLogin', async (event, data) => {
    let arrayData = JSON.parse(data);

    try {
        const user = await Users.findOne({ where: { username: arrayData.username } });
        
        if (!user) {
            return event.returnValue = JSON.stringify({'status': false, 'message': 'Usuario no encontrado'});
        }
        
        const match = await bcrypt.compare(arrayData.password, user.getDataValue('password'));
        
        if (match == false) {
            return event.returnValue = JSON.stringify({'status': false, 'message': 'Contraseña invalida'});
        }
        
        dataUser = { 'isAdmin': user.getDataValue('isAdmin'), 'name': user.getDataValue('name'), 'lastname': user.getDataValue('lastName'), 'rol': user.getDataValue('rol') };
        
        return event.returnValue = JSON.stringify({'status': true, 'message': JSON.stringify(user)});
    } catch (error) {
        return event.returnValue = JSON.stringify({'status': false, 'message': error});
    }
});

/**
 * Iniciando la session
 */
ipcMain.on('loginAction', async () => {
    try {
        const automotores = await Automotor.findAll();
        // const records = await Record.findAll({ where: { delete_at: null } });

        // for (const key in records) {
        //     let automotor = await Automotor.findByPk(records[key].getDataValue('automotor'));
        //     let rate = await Rates.findByPk(records[key].getDataValue('rate'));

        //     records[key].setDataValue('automotor', automotor.getDataValue('automotor_type'));
        //     records[key].setDataValue('rate', rate.getDataValue('type_rate'));
        // }

        return loadTemplates('src/templates/index.html.twig', {
            'automotores': automotores,
            // 'records': records,
            'dataUser': dataUser
        });
    } catch (error) {
        console.log(error);
    }
});

/**
 * Cerrando la session del usuario
 */
ipcMain.on('logout', () => {
    dataUser = {};

    return loadTemplates('src/templates/login.html.twig', {
        'login': true
    });
});

/**
 * Verificando tarifas por automotor
 */
ipcMain.on('consultRatesForAutomotor', async (event, arg) => {
    let data = JSON.parse(arg);

    try {
        const automotor = await Automotor.findByPk(data.automotorId);
        const rates = await Rates.findAll({ where: { automotor_type: automotor.getDataValue('id') } });
        
        return event.returnValue = {'status': 200, 'automotor': JSON.stringify(automotor), 'rates': JSON.stringify(rates)};
    } catch (error) {
        return event.returnValue = {'status': 500, 'msg': error};
    }
});

/**
 * Lista de tarifas
 */
ipcMain.on('ratesList', async (event) => {
    try {
        const ratesList = await Rates.findAll();

        for (const key in ratesList) {
            let automotor = await Automotor.findByPk(ratesList[key].getDataValue('automotor_type'));
            ratesList[key].setDataValue('automotor_type', automotor.getDataValue('automotor_type'));
        }

        return loadTemplates('src/templates/rateList.html.twig', {
            'rates': ratesList,
            'dataUser': dataUser
        });
    } catch (error) {
        console.log(error);
    }
});

/**
 * Registrar nueva tarifa
 */
ipcMain.on('newRate', async (event) => {
    const automotores = await Automotor.findAll();
    
    return loadTemplates('src/templates/newRate.html.twig', {
        'automotores': automotores,
        'dataUser': dataUser
    });
});

/**
 * Creacion de la tarifa
 */
ipcMain.on('createRate', async (event, arg) => {
    let data = JSON.parse(arg);

    try {
        const automotor = await Automotor.findByPk(data.automotor);
        const rate = await Rates.create({
            automotor_type: automotor.getDataValue('id'),
            type_rate: data.typeRate,
            value_rate: data.rateValue
        });
        
        rate.save();

        return event.returnValue = {'status': 201, 'msg': 'Nueva tarifa generada'};
    } catch (error) {
        return event.returnValue = {'status': 500, 'msg': error};
    }
});

/**
 * Eliminacion de una tarifa
 */
ipcMain.on('deleteRate', async (event, arg) => {
    let data = JSON.parse(arg);

    try {
        await Rates.destroy({
            where: { id: data.id },
            force: true
        });

        return event.returnValue = {'status': 200, 'msg': 'Registro de tarifa eliminado'};
    } catch (error) {
        return event.returnValue = {'status': 500, 'msg': 'Ha ocurrido un error inesperado: '+ error};
    }
});

/**
 * formulario donde cargara todos los datos de una tarifa
 */
ipcMain.on('editRate', async (event, arg) => {
    let data = JSON.parse(arg);
    const rate = await Rates.findByPk(data.id);
    const automotores = await Automotor.findAll();
    
    return loadTemplates('src/templates/rateEdit.html.twig', {
        'rate': rate,
        'automotores': automotores,
        'dataUser': dataUser
    });
});

/**
 * Actualizacion de datos por tarifa
 */
ipcMain.on('updateRate', async (event, arg) => {
    let data = JSON.parse(arg);

    try {
        const rate = await Rates.findByPk(data.id);

        rate.setDataValue('automotor_type', data.automotorId);
        rate.setDataValue('type_rate', data.typeRate);
        rate.setDataValue('value_rate', data.valueRate);

        rate.save();

        return event.returnValue = {'status': 200, 'msg': 'Registro actualizado'}
    } catch (error) {
        return event.returnValue = {'status': 500, 'msg': 'Ha ocurrido un error inesperado: '+ error};
    }
})

/**
 * Lista de automotores
 */
ipcMain.on('automotorList', async () => {
    try {
        const automotoresList = await Automotor.findAll();

        return loadTemplates('src/templates/automotorList.html.twig', {
            'automotores': automotoresList,
            'dataUser': dataUser
        });
    } catch (error) {
        console.log(error);
    }
});

/**
 * Registrar nuevo automotor
 */
ipcMain.on('newAutomotor', () => {
    return loadTemplates('src/templates/newAutomotor.html.twig', {
        'dataUser': dataUser
    });
});

/**
 * Validacion y creacion del nuevo automotor
 */
ipcMain.on('createAutomotor', async (event, arg) => {
    let data = JSON.parse(arg);

    try {
        const [automotor, statusCreate] = await Automotor.findOrCreate({
            where: { automotor_type: data.automotorName },
            defaults: {
                automotor_type: data.automotorName,
                automotor_items: data.statusItem == true ? data.item : null,
                enabled_items: data.statusItem
            }
        });

        if (!statusCreate) {
            return event.returnValue = {'status': 302, 'msg': 'El automotor ya esta registrado'}
        }

        return event.returnValue = {'status': 201, 'msg': 'Nuevo automotor generado'};
    } catch (error) {
        return event.returnValue = {'status': 500, 'msg': error}
    }
});

/**
 * Actualizando los datos por automotor
 */
ipcMain.on('updateAutomotor', async (event, arg) => {
    let data = JSON.parse(arg);

    try {
        const automotor = await Automotor.findByPk(data.id);

        automotor.setDataValue('automotor_type', data.automotorName);
        automotor.setDataValue('automotor_items', data.item);
        automotor.setDataValue('enabled_items', data.statusItem);

        automotor.save();

        return event.returnValue = {'status': 200, 'msg': 'Registro actualizado'}
    } catch (error) {
        return event.returnValue = {'status': 500, 'msg': 'Ha ocurrido un error inesperado: '+ error};
    }
});

/**
 * Registrando un nuevo ingreso
 */
ipcMain.on('registerEntry', async (event, arg) => {
    let data = JSON.parse(arg);
    let userData = JSON.parse(data.userData);

    try {
        const user = await Users.findByPk(userData.user_id);
        const automotor = await Automotor.findByPk(data.automotor);
        const rate = await Rates.findByPk(data.rate);

        let cantItems = null;
        if (data.cantItems != '') {
            cantItems = data.cantItems;
        }
        
        const record = await Record.create({
            id: uuidv4(),
            user_employed: user.getDataValue('user_id'),
            automotor: automotor.getDataValue('id'),
            cant_automotor_items: cantItems,
            rate: rate.getDataValue('id'),
            plate: data.plate,
            start_date_time: new Date(data.startDateTime)
        });
        
        record.save();

        record.setDataValue('automotor', automotor.getDataValue('automotor_type'));
        record.setDataValue('rate', rate.getDataValue('type_rate'));

        printEntryRecord(record, automotor, rate);
        
        return event.returnValue = {'status': 201, 'msg': 'Nuevo ingreso registrado'};
    } catch (error) {
        return event.returnValue = {'status': 500, 'msg': error};
    }
});

/**
 * Eliminando el registro de automotor
 */
ipcMain.on('deleteAutomotor', async(event, arg) => {
    let data = JSON.parse(arg);

    try {
        // Se elimina las tarifas que estuvieron asociadas al automotor
        await Rates.destroy({
            where: { automotor_type: data.autoId },
            force: true
        });

        await Automotor.destroy({
            where: { id: data.autoId },
            force: true
        });

        return event.returnValue = {'status': 200, 'msg': 'Registro de automotor eliminado'};
    } catch (error) {
        return event.returnValue = {'status': 500, 'msg': 'Ha ocurrido un error inesperado'};
    }
});

/**
 * Consultar datos registro de automotor para editar
 */
ipcMain.on('editAutomotor', async (event, arg) => {
    let data = JSON.parse(arg);
    const automotor = await Automotor.findByPk(data.autoId);

    return loadTemplates('src/templates/automotorEdit.html.twig', {
        'automotor': automotor,
        'dataUser': dataUser
    });
})

/**
 * Se buscan todos los registros de ingresos
 */
ipcMain.on('searchRecords', async (event, arg) => {
    let data = JSON.parse(arg);

    try {
        const records = data.plate != '' ? 
            await Record.findAll({ where: { plate: data.plate, delete_at: null }}) : 
            await Record.findAll({limit: 6, offset: parseInt(data.offset), order: [['start_date_time', 'DESC']], where: { delete_at : null }});
        
        for (const key in records) {
            let automotor = await Automotor.findByPk(records[key].getDataValue('automotor'));
            let rate = await Rates.findByPk(records[key].getDataValue('rate'));

            records[key].setDataValue('automotor', automotor.getDataValue('automotor_type'));
            records[key].setDataValue('rate', rate.getDataValue('type_rate'));

            let startDate = new Date(records[key].getDataValue('start_date_time'));
            records[key].setDataValue('start_date_time', startDate.getDate() +'/'+ (startDate.getMonth() + 1) +'/'+ startDate.getFullYear() +' '+ formatAMPM(startDate));
        }
        
        return event.returnValue = {'status': 200, 'msg': JSON.stringify(records)};
    } catch (error) {
        return event.returnValue = {'status': 500, 'msg': error};
    }
});



/**
 * Volver al index
 */
ipcMain.on('returnToIndex', async () => {

    try {
        const automotores = await Automotor.findAll();
        
        return loadTemplates('src/templates/index.html.twig', {
            'automotores': automotores,
            'dataUser': dataUser
        });
    } catch (error) {
        console.log(error);
    }
});

/**
 * Muestra informacion sobre el ingreso e impresion
 */
ipcMain.on('checkRecordInList', async (event, arg) => {
    let data = JSON.parse(arg);

    try {
        const record = await Record.findByPk(data.recordId);

        let automotor = await Automotor.findByPk(record.getDataValue('automotor'));
        let rate = await Rates.findByPk(record.getDataValue('rate'));

        record.setDataValue('automotor', automotor.getDataValue('automotor_type'));
        record.setDataValue('rate', rate.getDataValue('type_rate'));
        
        let amount = getTotalAmountToRate(record, rate);
        
        let startDate = new Date(record.getDataValue('start_date_time'));
        let endDate = new Date(record.getDataValue('end_date_time'));

        record.setDataValue('start_date_time', startDate.getDate() +'/'+ (startDate.getMonth() + 1) +'/'+ startDate.getFullYear() +' '+ formatAMPM(startDate));
        record.setDataValue('end_date_time', record.getDataValue('end_date_time') != null ? endDate.getDate() +'/'+ (endDate.getMonth() + 1) +'/'+ endDate.getFullYear() +' '+ formatAMPM(endDate) : null);
        
        winInfoToRecord = new BrowserWindow({
            width: 1000,
            height: 700,
            webPreferences: { nodeIntegration: true, contextIsolation: false }
        });

        winInfoToRecord.removeMenu();
        // winInfoToRecord.webContents.openDevTools();
        winInfoToRecord.loadURL(url.format({
            pathname: path.join(__dirname, 'src/templates/infoToRecord.html.twig'),
            protocol: 'file',
            slashes: true
        }));

        twig.view = { 'record': record, 'amount': amount, 'rate': rate, 'login': true  }
    } catch (error) {
        console.log(error);
    }
});

/**
 * Registrar la salida
 */
ipcMain.on('recordToExit', async(event, arg) => {
    let data = JSON.parse(arg);

    const record = await Record.findByPk(data.recordId);
    const user = await Users.findByPk(data.user_id);

    const box = await Box.create({
        user: user.getDataValue('user_id'),
        amount: data.amount,
        regist_date: getCurrentDate()
    });

    box.save();

    record.setDataValue('end_date_time', getCurrentDate());
    record.save();

    let automotor = await Automotor.findByPk(record.getDataValue('automotor'));
    let rate = await Rates.findByPk(record.getDataValue('rate'));

    record.setDataValue('automotor', automotor.getDataValue('automotor_type'));
    record.setDataValue('rate', rate.getDataValue('type_rate'));

    winInfoToRecord.close();

    printExitRecord(record, automotor, rate, data);
});

/**
 * Listado de usuarios y acceso a registro de usuarios
 */
ipcMain.on('usersList', async () => {
    try {
        const users = await Users.findAll({ where: { [Op.or]: [ { rol: 'Administrador' }, { rol: 'Empleado' } ] } });

        return loadTemplates('src/templates/userList.html.twig', {
            'users': users,
            'dataUser': dataUser
        });
    } catch (error) {
        console.log(error);
    }
});

/**
 * Formulario de registro de usuario
 */
ipcMain.on('newUser', async () => {
    return loadTemplates('src/templates/newUser.html.twig', {
        'dataUser': dataUser
    });
});

/**
 * Verificar la existencia del username
 */
ipcMain.on('checkUsername', async (event, arg) => {
    let data = JSON.parse(arg);

    try {
        const user = await Users.findOne({ where: { username: data.username } });
        
        if (user != null) {
            return event.returnValue = {'status': true, 'msg':'El nombre de usuario ya esta siendo utilizado'};
        }

        return event.returnValue = {'status': false, 'msg': 'ok'};
    } catch (error) {
        return event.returnValue = {'status': false, 'msg': 'Ha ocurrido un error inesperado: '+ error};
    }
});

/**
 * Creacion de usuarios
 */
ipcMain.on('createUser', async (event, arg) => {
    const data = JSON.parse(arg);

    try {
        const password = bcrypt.hashSync(data.password, 10);

        const user = await Users.create({
            user_id: uuidv4(),
            name: data.name,
            lastName: data.lastname,
            username: data.username,
            rol: data.rol == 1 ? 'Administrador' : 'Empleado',
            password: password,
            isAdmin: data.rol == 1 ? true : false,
            createdAt: getCurrentDate(),
            updatedAt: getCurrentDate()
        });
        
        user.save();

        return event.returnValue = {'status': 201, 'msg': data.name + ' registrado exitosamente'}
    } catch (error) {
        return event.returnValue = {'status': 500, 'msg': 'Ha ocurrido un error inesperado: '+ error}
    }
});

/**
 * Eliminacion de usuarios
 */
ipcMain.on('deleteUser', async (event, arg) => {
    const data = JSON.parse(arg);

    try {
        await Users.destroy({
            where: { user_id: data.id },
            force: true
        });

        return event.returnValue = {'status': 200, 'msg': 'El usuario fue removido exitosamente'};
    } catch (error) {
        return event.returnValue = {'status': 500, 'msg': 'Ha ocurrido un error inesperado: '+ error};
    }
});

/**
 * Formulario para editar el usuario
 */
ipcMain.on('editUser', async (event, arg) => {
    const data = JSON.parse(arg);
    const user = await Users.findByPk(data.id);

    return loadTemplates('src/templates/userEdit.html.twig', { 
        'user': user,
        'dataUser': dataUser
    });
});

/**
 * Actualizar datos del Usuario
 */
ipcMain.on('updateUser', async (event, arg) => {
    const data = JSON.parse(arg);

    try {
        const user = await Users.findByPk(data.id);

        user.setDataValue('name', data.name);
        user.setDataValue('lastName', data.lastname);
        user.setDataValue('username', data.username);
        user.setDataValue('rol', data.rol == 1 ? 'Administrador' : 'Empleado');
        user.setDataValue('isAdmin', data.rol == 1 ? true : false);
        user.setDataValue('updatedAt', getCurrentDate());

        if(data.password != ''){
            const password = bcrypt.hashSync(data.password, 10);
            user.setDataValue('password', password);
        }
        
        user.save();

        return event.returnValue = {'status': 200, 'msg': 'Información del usuario, correctamente actualizada'}
    } catch (error) {
        return event.returnValue = {'status': 500, 'msg': 'Ha ocurrido un error inesperado: '+ error}
    }
});

ipcMain.on('reports', async () => {

    const automotores = await Automotor.findAll();
    const clients = await Client.findAll();
    const reports = await Report.findAll({ where: { delete_at: null } });

    for (let i = 0; i < reports.length; i++) {
        let interval = compareTwoDates(new Date(reports[i].getDataValue('report_end_date')), getCurrentDate());
        let intervalDays = interval.days;
        let days = intervalDays.toString().includes('-');

        reports[i].days = days == true ? 0 : intervalDays;
    }
    
    return loadTemplates('src/templates/reportsList.html.twig', {
        'automotores': automotores,
        'clients': clients,
        'reports': reports,
        'dataUser': dataUser
    });
});

ipcMain.on('newReport', async(event, arg) => {
    const data = JSON.parse(arg);
    let client;
    
    try {   
        const automotor = await Automotor.findByPk(data.automotor); 

        if (data.client_id != '') {
            client = await Client.findByPk(data.client_id);
        }else{
            client = await Client.create({
                id: uuidv4(),
                full_name: data.client_full_name,
                document: data.client_document,
                phone: data.client_phone,
                createdAt: getCurrentDate(),
                updatedAt: getCurrentDate()
            });

            client.save();
        }

        const report = await Report.create({
            client: client.getDataValue('id'),
            automotor: automotor.getDataValue('id'),
            plate: data.plate,
            rate: data.rate,
            report_start_date: new Date(data.date_entry),
            report_end_date: new Date(data.date_payment),
            status: true
        });

        report.save();

        return event.returnValue = {'status': 201, 'msg': 'Informe creado y cliente registrado'}
    } catch (error) {
        return event.returnValue = {'status': 500, 'msg': 'Ha ocurrido un error inesperado: '+ error}
    }
});

ipcMain.on('searchClient', async(event, arg) => {
    let data = JSON.parse(arg);

    try {
        $sql = "SELECT * FROM Clients WHERE full_name LIKE '%"+ data.term +"%'";
        const clients = await db.sequelize.query($sql, { model: Client });

        return event.returnValue = {'status': 200, 'clients': clients }
    } catch (error) {
        return event.returnValue = {'status': 500, 'msg': 'Ha ocurrido un error inesperado: '+ error}
    }
});

ipcMain.on('reportExit', async(event, arg) => {
    const data = JSON.parse(arg);

    const report = await Report.findByPk(data.reportId);
    const automotor = await Automotor.findByPk(report.getDataValue('automotor'));
    const cliente = await Client.findByPk(report.getDataValue('client'));
    
    let interval = compareTwoDates(new Date(report.getDataValue('report_end_date')), getCurrentDate());
    let intervalDays = interval.days;
    let days = intervalDays.toString().includes('-');
    days = days == true ? 0 : intervalDays;

    if (days > 0) {
        let amountArray = report.getDataValue('rate').split('.');
        let amount = '';
        
        for (let i = 0; i < amountArray.length; i++) {
            amount += amountArray[i];            
        }
        
        let totalAmount = Number(amount) + (Number(amount) / 30) * days;
        report.setDataValue('rate', Math.ceil(totalAmount).toString());
    }

    report.setDataValue('rate', addPoints(report.getDataValue('rate')).split(',')[0]);

    let responseData = { 'report': report, 'automotor': automotor, 'cliente': cliente };

    return event.returnValue = JSON.stringify(responseData);
});

ipcMain.on('finalizeMonthly', async (event, arg) => {
    const data = JSON.parse(arg);

    try {
        const report = await Report.findByPk(data.id);
        const automotor = await Automotor.findByPk(report.getDataValue('automotor'));
        const client = await Client.findByPk(report.getDataValue('client'));
        const user = await Users.findByPk(data.userId);

        const lastInitDate = report.getDataValue('report_start_date');
        const lastEndDate = report.getDataValue('report_end_date');
        
        if (data.enabledNextCharge == true) {
            let currentDate = getCurrentDate();
            currentDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate(), 0, 0, 0, 0);
            currentDate.setUTCHours(0);

            let nextDate = getCurrentDate();
            nextDate.setDate(nextDate.getDate() + 30);
            nextDate = new Date(nextDate.getFullYear(), nextDate.getMonth(), nextDate.getDate(), 0, 0, 0, 0);
            nextDate.setUTCHours(0);
            
            report.setDataValue('report_start_date', currentDate);
            report.setDataValue('report_end_date', nextDate);
        }else{
            report.setDataValue('status', false);
        }
        
        report.save();

        const box = await Box.create({
            user: user.getDataValue('user_id'),
            amount: data.paymentMonth,
            regist_date: getCurrentDate()
        });

        box.save();
        
        printReportExit({
            'report': report, 
            'automotor': automotor, 
            'client': client, 
            'paymentMonth': data.paymentMonth, 
            'paymentReceived': data.paymentReceived, 
            'enabledNextCharge': data.enabledNextCharge,
            'lastInitDate': lastInitDate,
            'lastEndDate': lastEndDate
        });

        return event.returnValue = {'status': 200};
    } catch (error) {
        return event.returnValue = {'status': 500, 'msg': 'Ha ocurrido un error inesperado: '+ error};
    }
});

ipcMain.on('reportEdit', async (event, arg) => {
    const data = JSON.parse(arg);
    const report = await Report.findByPk(data.reportId);

    if (typeof data.renove == 'undefined') {
        if (report.getDataValue('status') == false || !report) {
            return event.returnValue = {'status': 501};    
        }
    }

    return event.returnValue = {'status': 200, 'report': report};
});

ipcMain.on('reportUpdate', async(event, arg) => {
    const data = JSON.parse(arg);

    try {
        const report = await Report.findByPk(data.reportId);

        report.setDataValue('report_start_date', new Date(data.newStartDate));
        report.setDataValue('report_end_date', new Date(data.newEndDate));

        report.save();

        return event.returnValue = {'status': 200, 'msg': 'Datos actualizados'}; 
    } catch (error) {
        return event.returnValue = {'status': 500, 'msg': 'Ha ocurrido un error inesperado: '+ error};
    }
});

ipcMain.on('renoveReport', async(event, args) => {
    const data = JSON.parse(args);

    try {
        const report = await Report.findByPk(data.id);

        report.setDataValue('report_start_date', new Date(data.renoveStartDate));
        report.setDataValue('report_end_date', new Date(data.renoveEndDate));
        report.setDataValue('rate', data.renoveNewRate);
        report.setDataValue('status', true);

        report.save();

        return event.returnValue = {'status': 200, 'msg': 'Mensaulidad modificada y reingresada'}
    } catch (error) {
        return event.returnValue = {'status': 500, 'msg': 'Ha ocurrido un error inesperado: '+ error}
    }
});

ipcMain.on('reportDelete', async(event, args) => {
    const data = JSON.parse(args);

    try {
        await Report.destroy({
            where: { id: data.reportId },
            force: true
        });

        return event.returnValue = {'status': 200, 'msg': 'Reporte eliminado'}; 
    } catch (error) {
        return event.returnValue = {'status': 500, 'msg': 'Ha ocurrido un error inesperado: '+ error};
    }
});

ipcMain.on('box', async () => {
    const box = await Box.findAll();
    let currentDate = getCurrentDate();
    let auxCurrentDate = currentDate.toISOString().split('T')[0];
    auxCurrentDate = auxCurrentDate.substring(8,10) +'/'+ auxCurrentDate.substring(5,7) +'/'+ auxCurrentDate.substring(0,4);
    
    let dailyCashReport = {};
    let dailyTotalAmount = 0;

    for (let i = 0; i < box.length; i++) {
        let dateRegist = new Date(box[i].getDataValue('regist_date'));
        let auxDateRegist = dateRegist.toISOString().split('T')[0];
        auxDateRegist = auxDateRegist.substring(8,10) +'/'+ auxDateRegist.substring(5,7) +'/'+ auxDateRegist.substring(0,4);

        let user = await Users.findByPk(box[i].getDataValue('user'));
        
        if (auxCurrentDate == auxDateRegist) {
            let amountArray = box[i].getDataValue('amount').split('.');
            let amountClean = '';

            for (let x = 0; x < amountArray.length; x++) { amountClean += amountArray[x]; }
            dailyTotalAmount += Number(amountClean);

            dailyCashReport[i] = {
                'user': {
                    'name': user.getDataValue('name'),
                    'lastname': user.getDataValue('lastName'),
                    'rol': user.getDataValue('rol')
                },
                'amount': box[i].getDataValue('amount')
            };
        }
    }

    const dataMonth = getDataAmountPerMonth(box, currentDate);
    
    return loadTemplates('src/templates/box.html.twig', {
        'dailyCashReport': dailyCashReport,
        'dailyTotalAmount': addPoints(dailyTotalAmount.toString()).split(',')[0],
        'dataMonth': dataMonth,
        'currentDate': currentDate,
        'dataUser': dataUser
    });
});

ipcMain.on('confirmAccount', async(event, arg) => {
    const data = JSON.parse(arg);

    try {
        const user = await Users.findOne({ where: { username: data.username } });
        
        if (!user) {
            return event.returnValue = {'status': 401, 'message': 'Usuario no encontrado'};
        }
        
        const match = await bcrypt.compare(data.password, user.getDataValue('password'));
        
        if (match == false) {
            return event.returnValue = {'status': 401, 'message': 'Los datos subministrados no coinciden'};
        } 
        
        if (user.getDataValue('isAdmin') == true && (user.getDataValue('rol') == 'Administrador' || user.getDataValue('rol') == 'SuperUser')) {
            return event.returnValue = {'status': 200, 'message': 'Datos correctos'};
        }else{
            return event.returnValue = {'status': 400, 'message': 'Acceso denegado'};
        }
    } catch (error) {
        return event.returnValue = {'status': 500, 'message': 'Ha ocurrido un error inesperado: '+ error};
    }
});

ipcMain.on('chooseDirectoryBackup', async (event) => {

    try {
        const responseDirectory = await dialog.showOpenDialog({ 
            title: 'Seleccione carpeta donde almacenar la copia de seguridad',
            properties: ['openDirectory'] 
        });
    
        if (responseDirectory.canceled == false && responseDirectory.filePaths != []) {
            const directory = responseDirectory.filePaths[0] + '/parkinglotdb.db';
            const dbStorage = './'+ db.routedb;

            fs.copyFile(dbStorage, directory, fs.constants.COPYFILE_FICLONE, (err) => {
                if (err) throw err;
                if (err == null) {
                    return event.returnValue = {'status': 200, 'message': 'Copia creada en el directorio: '+ directory};
                }
            });
        }else if (responseDirectory.canceled == true){
            return event.returnValue = {'status': 200, 'message': 'Creacion de copia cancelada'};
        }else{
            return event.returnValue = {'status': 400, 'message': 'Ruta desconocida o no reconocida adecuadamente'};
        }
    } catch (error) {
        return event.returnValue = {'status': 500, 'message': 'Ha ocurrido un error inesperado: '+ error};
    }
});

ipcMain.on('selectFileBackup', async (event) => {
    try {
        const file = await dialog.showOpenDialog({ 
            title: 'Seleccione copia a montar',
            properties: ['openFile'],
            filters: [
                {name: 'database', extensions: ['db']}
            ]
        });

        if (file.canceled == false && file.filePaths != []) {
            const filename = path.basename(file.filePaths[0]);
            let bdName = db.routedb;
            let arrayBdName = bdName.split('/');
            let limitArray = arrayBdName.length - 1;

            if (filename != arrayBdName[limitArray]) {
                return event.returnValue = {'status': 400, 'message': 'La copia que intenta subir, no es una base de datos del sistema'};
            }

            return event.returnValue = {'status': 201, 'message': 'Copia de seguridad cargada, está seguro de proceder?', 'file': file.filePaths[0]};
        }else if (file.canceled == true){
            return event.returnValue = {'status': 200, 'message': 'Operacion cancelada'};
        }else{
            return event.returnValue = {'status': 400, 'message': 'Ruta desconocida o no reconocida adecuadamente'};
        }

    } catch (error) {
        return event.returnValue = {'status': 500, 'message': 'Ha ocurrido un error inesperado: '+ error};
    }
});

ipcMain.on('pushBackUp', async (event, arg) => {
    const data = JSON.parse(arg);
    const file = data.file;
    const directory = './'+ db.routedb;

    fs.copyFile(file, directory, fs.constants.COPYFILE_FICLONE, (err) => {
        if (err) throw err;
        if (err == null) {
            return event.returnValue = {'status': 200, 'message': 'Copia de seguridad montada exitosamente'};
        }
    });
});

ipcMain.on('supportIndex', async (event) => {
    const dateLessMonth = getCurrentDate();
    dateLessMonth.setMonth(dateLessMonth.getMonth() - 1);
    
    const recordsTrapped = await Record.findAll({ where: { end_date_time: { [Op.lte]: dateLessMonth }, delete_at: null } });
    
    return loadTemplates('src/templates/supportIndex.html.twig', {
        'records': recordsTrapped
    });
});

ipcMain.on('checkForDatabaseCleaning', async(event, args) => {
    try {
        getDataFromCleaning().then((arrayData) => {
            let countRecords = arrayData.records.length;
            let countReports = arrayData.reports.length;

            if (countRecords > 0 || countReports > 0) {
                return event.returnValue = {status: 204, message: "Se han encontrado datos viejos, ¿desea realizar una limpieza de sus datos?"};
            }

            return event.returnValue = {status: 200, message: "NaN"};
        });
    } catch (error) {
        return event.returnValue = {status: 500, message: 'Ha ocurrido un error inesperado: '+ error};
    }    
});

ipcMain.on('cleaningDatabaseAction', async(event, args) => {
    try {
        getDataFromCleaning().then((arrayData) => {
            let recordsTrapped = arrayData.records;
            let reportTrapped = arrayData.reports;
            
            let countRecords = recordsTrapped.length;
            let countReports = reportTrapped.length;
            
            recordsTrapped.forEach(record => {
                if (record.getDataValue('end_date_time') == null) {
                    record.setDataValue('end_date_time', getCurrentDate());
                }

                record.setDataValue('delete_at', getCurrentDate());
                record.save();
            });
            
            reportTrapped.forEach(report => {
                report.setDataValue('delete_at', getCurrentDate());
                report.save();
            });

            return event.returnValue = {status: 200, message: "Registros eliminados: "+ (countRecords + countReports) + " listados optimizados, actualizando sección" };
        });
    } catch (error) {
        return event.returnValue = {status: 500, message: 'Ha ocurrido un error inesperado: '+ error};
    }
})

async function getDataFromCleaning() {
    const dateLessMonth = getCurrentDate();

    // Consultando ingresos particulares con un mes de finalizado
    dateLessMonth.setMonth(dateLessMonth.getMonth() - 2);
    const recordsTrapped = await Record.findAll({ where: { start_date_time: { [Op.lte]: dateLessMonth }, delete_at: null } });

    // Consultando ingresos de mensualidades con tres meses de finalizado
    dateLessMonth.setMonth(dateLessMonth.getMonth() - 1);
    const reportTrapped = await Report.findAll({ where: { report_end_date: { [Op.lte]: dateLessMonth }, status: false, delete_at: null} });
    
    return {'records': recordsTrapped, 'reports': reportTrapped};
}

function getDataAmountPerMonth(info, currentDate) { 
    let information = {};
    let amountClean;
    let amountArray;
    let amountEnero = 0;
    let amountFebrero = 0;
    let amountMarzo = 0;
    let amountAbril = 0;
    let amountMayo = 0;
    let amountJunio = 0;
    let amountJulio = 0;
    let amountAgosto = 0;
    let amountSeptiembre = 0;
    let amountOctubre = 0;
    let amountNoviembre = 0;
    let amountDiciembre = 0;

    for (let i = 0; i < info.length; i++) {
        let registDate = new Date(info[i].regist_date);

        if (currentDate.getFullYear() == registDate.getFullYear()) {
            let month = registDate.getMonth() + 1;
            
            switch (month) {
                case 1:
                    amountArray = info[i].amount.split('.');
                    amountClean = '';

                    for (let x = 0; x < amountArray.length; x++) { amountClean += amountArray[x]; }
                    amountEnero += Number(amountClean);

                    information['Enero'] = {'amount': amountEnero, 'color': '200, 77, 77'};
                break;
                case 2:
                    amountArray = info[i].amount.split('.');
                    amountClean = '';

                    for (let x = 0; x < amountArray.length; x++) { amountClean += amountArray[x]; }
                    amountFebrero += Number(amountClean);

                    information['Febrero'] = {'amount': amountFebrero, 'color': '192, 153, 74'};
                break;
                case 3:
                    amountArray = info[i].amount.split('.');
                    amountClean = '';

                    for (let x = 0; x < amountArray.length; x++) { amountClean += amountArray[x]; }
                    amountMarzo += Number(amountClean);

                    information['Marzo'] = {'amount': amountMarzo, 'color': '105, 209, 33'};
                break;
                case 4:
                    amountArray = info[i].amount.split('.');
                    amountClean = '';

                    for (let x = 0; x < amountArray.length; x++) { amountClean += amountArray[x]; }
                    amountAbril += Number(amountClean);

                    information['Abril'] = {'amount': amountAbril, 'color': '33, 103, 209'};
                break;
                case 5:
                    amountArray = info[i].amount.split('.');
                    amountClean = '';

                    for (let x = 0; x < amountArray.length; x++) { amountClean += amountArray[x]; }
                    amountMayo += Number(amountClean);

                    information['Mayo'] = {'amount': amountMayo, 'color': '43, 33, 191'};
                break;
                case 6:
                    amountArray = info[i].amount.split('.');
                    amountClean = '';

                    for (let x = 0; x < amountArray.length; x++) { amountClean += amountArray[x]; }
                    amountJunio += Number(amountClean);

                    information['Junio'] = {'amount': amountJunio, 'color': '214, 46, 143'};
                break;
                case 7:
                    amountArray = info[i].amount.split('.');
                    amountClean = '';

                    for (let x = 0; x < amountArray.length; x++) { amountClean += amountArray[x]; }
                    amountJulio += Number(amountClean);

                    information['Julio'] = {'amount': amountJulio, 'color': '34, 214, 97'};
                break;
                case 8:
                    amountArray = info[i].amount.split('.');
                    amountClean = '';

                    for (let x = 0; x < amountArray.length; x++) { amountClean += amountArray[x]; }
                    amountAgosto += Number(amountClean);

                    information['Agosto'] = {'amount': amountAgosto, 'color': '21, 224, 227'};
                break;
                case 9:
                    amountArray = info[i].amount.split('.');
                    amountClean = '';

                    for (let x = 0; x < amountArray.length; x++) { amountClean += amountArray[x]; }
                    amountSeptiembre += Number(amountClean);

                    information['Septiembre'] = {'amount': amountSeptiembre, 'color': '231, 241, 12'};
                break;
                case 10:
                    amountArray = info[i].amount.split('.');
                    amountClean = '';

                    for (let x = 0; x < amountArray.length; x++) { amountClean += amountArray[x]; }
                    amountOctubre += Number(amountClean);

                    information['Octubre'] = {'amount': amountOctubre, 'color': '241, 12, 12'};
                break;
                case 11:
                    amountArray = info[i].amount.split('.');
                    amountClean = '';

                    for (let x = 0; x < amountArray.length; x++) { amountClean += amountArray[x]; }
                    amountNoviembre += Number(amountClean);

                    information['Noviembre'] = {'amount': amountNoviembre, 'color': '241, 89, 12'};
                break;
                case 12:
                    amountArray = info[i].amount.split('.');
                    amountClean = '';

                    for (let x = 0; x < amountArray.length; x++) { amountClean += amountArray[x]; }
                    amountDiciembre += Number(amountClean);

                    information['Diciembre'] = {'amount': amountDiciembre, 'color': '0, 63, 220'};
                break;
            }
        }
    }

    return information;
}

function loadTemplates(pathTemplate, data = {}) {
    win.loadURL(url.format({
        pathname: path.join(__dirname, pathTemplate),
        protocol: 'file',
        slashes: true
    }));

    twig.view = data;
}

function getTotalAmountToRate(record, rate){
    let prevTime = new Date(record.start_date_time);
    let currentDate = record.end_date_time == null ? getCurrentDate() : new Date(record.end_date_time);
    let interval = compareTwoDates(prevTime, currentDate);
    let amount = 0;

    rate.value_rate = rate.value_rate.replace('.','');
    switch (record.rate) {
        case 1: amount = interval.hours < 1 ? Number(rate.value_rate) : (interval.hours * Number(rate.value_rate));
            break;
        case 2: amount = interval.days < 1 ? Number(rate.value_rate) : (interval.days * Number(rate.value_rate));
            break;
        case 3: amount = interval.days < 1 ? Number(rate.value_rate) / 8 : (Number(rate.value_rate) / 8) * interval.days;
            break;
        case 4: amount = interval.days < 1 ? Number(rate.value_rate) / 30 : (Number(rate.value_rate) / 30) * interval.days;
            break;
    }

    return Math.ceil(amount);
}

async function printReportExit(args) {  
    const automotor = args.automotor;
    const client = args.client;
    const report = args.report;
    
    let paymentDate = parseDateDayMonthYear(getCurrentDate());
    let proxInitDate = parseDateDayMonthYear(report.report_start_date);
    let proxEndingDate = parseDateDayMonthYear(report.report_end_date);
    let initDate = parseDateDayMonthYear(args.lastInitDate);
    let endingDate = parseDateDayMonthYear(args.lastEndDate);

    let paymentMonth = args.paymentMonth.split('.');
    let paymentReceived = args.paymentReceived.split('.');
    let paymentMonthClean = '';
    let paymentReceivedClean = '';

    for (let i = 0; i < paymentMonth.length; i++) { paymentMonthClean += paymentMonth[i]; }
    for (let i = 0; i < paymentReceived.length; i++) { paymentReceivedClean += paymentReceived[i]; }

    let exchange = Number(paymentReceivedClean) - Number(paymentMonthClean);

    if (exchange > 0) {
        exchange = addPoints(exchange).split(',')[0];
    }

    let data = [
        { type: 'text', value: 'PARQUEADERO LA 12', style: 'font-size: 14px; font-weight: bold; margin: 0px 6px;' },
        { type: 'text', value: 'ALBERTO CASTILLO', style: 'font-size: 12px; margin: 0px 6px;' },
        { type: 'text', value: 'NIT. 41960573', style: 'font-size: 12px; margin: 0px 6px;' },
        { type: 'text', value: '', style: 'padding: 10% 0%;' },
        { type: 'text', value: 'CALLE 15 #12 - ESQUINA', style: 'font-size: 10px; margin: 0px 6px;' },
        { type: 'text', value: 'SERVICIO LAS 24 HORAS DEL DIA', style: 'font-size: 10px; margin: 0px 6px;' },
        { type: 'text', value: '===========================', style: 'font-size: 12px; margin: 0px 6px;' },
        { type: 'text', value: 'PAGO DE MENSUALIDAD', style: 'font-weight: bold; text-align: center; font-size: 12px; margin: 0px 6px;' },
        { type: 'text', value: 'AUTOMOTOR: '+ automotor.automotor_type, style: 'font-size: 12px; margin: 0px 6px;' },
        { type: 'text', value: 'CLIENTE: '+ client.full_name, style: 'font-size: 12px; margin: 0px 6px;' },
        { type: 'text', value: 'PLACA: '+ report.plate, style: 'font-size: 12px; margin: 0px 6px;' },
        { type: 'text', value: 'VALOR TAFIRA: '+ addPoints(report.rate).split(',')[0], style: 'font-size: 12px; margin: 0px 6px;' },
        { type: 'text', value: 'FECHA DE PAGO: '+ paymentDate, style: 'font-size: 12px; margin: 0px 6px;' },
        { type: 'text', value: '', style: 'padding: 3% 0%;' },
        { type: 'text', value: 'PERIODO DE PAGO', style: 'font-weight: bold; text-align: center; font-size: 12px; margin: 0px 6px;' },
        { type: 'text', value: 'DESDE: '+ initDate, style: 'font-size: 12px; margin: 0px 6px;' },
        { type: 'text', value: 'HASTA: '+ endingDate, style: 'font-size: 12px; margin: 0px 6px;' },
    ];

    if (args.enabledNextCharge) {
        data.push(
            { type: 'text', value: '', style: 'padding: 3% 0%;' },
            { type: 'text', value: 'PRÓXIMO PERIODO DE PAGO', style: 'font-weight: bold; text-align: center; font-size: 12px; margin: 0px 6px;' },
            { type: 'text', value: 'DESDE: '+ proxInitDate, style: 'font-size: 12px; margin: 0px 6px;' },
            { type: 'text', value: 'HASTA: '+ proxEndingDate, style: 'font-size: 12px; margin: 0px 6px;' },
        );
    }

    data.push(
        { type: 'text', value: '===========================', style: 'font-size: 12px; margin: 0px 6px;' },
        { type: 'text', value: 'TOTAL: '+ addPoints(paymentMonthClean.toString()).split(',')[0], style: 'font-size: 12px; margin: 0px 6px;' },
        { type: 'text', value: 'EFECTIVO: '+ addPoints(paymentReceivedClean.toString()).split(',')[0], style: 'font-size: 12px; margin: 0px 6px;' },
        { type: 'text', value: 'CAMBIO: '+ exchange, style: 'font-size: 12px; margin: 0px 6px;' },
        { type: 'text', value: '', style: 'padding: 5% 0%; margin: 0px 6px;' },
        { type: 'text', value: ticketDescription, style: 'font-size: 10px; text-align: justify; font-style: italic; margin: 0px 6px;' }
    );

    printer(data);
}

/**
 * Tiquete de entrada
 */
async function printEntryRecord(record, automotor, rate){
    let name = '';

    switch (rate.type_rate) {
        case 1: name = 'hora'; break;
        case 2: name = 'dia'; break;
        case 3: name = 'semana'; break;
        case 4: name = 'mes'; break;
    }
    
    let newStartDate = new Date(record.start_date_time);
    newStartDate = newStartDate.getDate() +'/'+ (newStartDate.getMonth() + 1) +'/'+ newStartDate.getFullYear() +' '+ formatAMPM(newStartDate);

    let data = [
        { type: 'text', value: 'PARQUEADERO LA 12', style: 'font-size: 14px; font-weight: bold; margin: 0px 6px;' },
        { type: 'text', value: 'ALBERTO CASTILLO', style: 'font-size: 12px; margin: 0px 6px;' },
        { type: 'text', value: 'NIT. 41960573', style: 'font-size: 12px; margin: 0px 6px;' },
        { type: 'text', value: '', style: 'padding: 10% 0%;' },
        { type: 'text', value: 'CALLE 15 #12 - ESQUINA', style: 'font-size: 10px; margin: 0px 6px;' },
        { type: 'text', value: 'SERVICIO LAS 24 HORAS DEL DIA', style: 'font-size: 10px; margin: 0px 6px;' },
        { type: 'text', value: '===========================', style: 'font-size: 12px; margin: 0px 6px;' },
        { type: 'text', value: 'AUTOMOTOR: '+ record.automotor, style: 'font-size: 12px; margin: 0px 6px;' },
    ];

    if (automotor.enabled_items == true) {
        data.push({ type: 'text', value: automotor.automotor_items +': '+ record.cant_automotor_items, style: 'font-size: 12px; margin: 0px 6px;' });
    }

    data.push(
        { type: 'text', value: 'VALOR TARIFA: '+ rate.value_rate +' por '+ name, style: 'font-size: 12px; margin: 0px 6px;' },
        { type: 'text', value: '', style: 'padding: 5% 0%;' },
        { type: 'text', value: record.plate, style: 'font-size: 34px; font-weight: bold; text-align: center; margin: 0px 6px;' },
        { type: 'text', value: '===========================', style: 'font-size: 12px; margin: 0px 6px;' },
        { type: 'text', value: 'FECHA Y HORA DE INGRESO: ', style: 'font-size: 12px; text-align: center; margin: 0px 6px;' },
        { type: 'text', value: newStartDate, style: 'font-size: 12px; text-align: center; margin: 0px 6px;' },
        { type: 'text', value: '', style: 'padding: 5% 0%;' },
        { type: 'text', value: ticketDescription, style: 'font-size: 10px; text-align: justify; font-style: italic; margin: 0px 6px;' },
    );
    
    printer(data);
}

/**
 * Tiquete de salida
 */
async function printExitRecord(record, automotor, rate, arg){
    let name = '';

    switch (rate.type_rate) {
        case 1: name = 'hora'; break;
        case 2: name = 'dia'; break;
        case 3: name = 'semana'; break;
        case 4: name = 'mes'; break;
    }

    let newStartDate = new Date(record.start_date_time);
    newStartDate = newStartDate.getDate() +'/'+ (newStartDate.getMonth() + 1) +'/'+ newStartDate.getFullYear() +' '+ formatAMPM(newStartDate);

    let newEndDate = new Date(record.end_date_time);
    newEndDate = newEndDate.getDate() +'/'+ (newEndDate.getMonth() + 1) +'/'+ newEndDate.getFullYear() +' '+ formatAMPM(newEndDate);

    let paymentAmount = arg.amount.split('.');
    let paymentReceived = arg.amountReceived.split('.');
    let paymentAmountClean = '';
    let paymentReceivedClean = '';

    for (let i = 0; i < paymentAmount.length; i++) { paymentAmountClean += paymentAmount[i]; }
    for (let i = 0; i < paymentReceived.length; i++) { paymentReceivedClean += paymentReceived[i]; }

    let exchange = Number(paymentReceivedClean) - Number(paymentAmountClean);

    if (exchange > 0) {
        exchange = addPoints(exchange).split(',')[0];
    }

    let data = [
        { type: 'text', value: 'PARQUEADERO LA 12', style: 'font-size: 14px; font-weight: bold; margin: 0px 6px;' },
        { type: 'text', value: 'ALBERTO CASTILLO', style: 'font-size: 12px; margin: 0px 6px;' },
        { type: 'text', value: 'NIT. 41960573', style: 'font-size: 12px; margin: 0px 6px;' },
        { type: 'text', value: '', style: 'padding: 10% 0%; margin: 0px 6px;' },
        { type: 'text', value: 'CALLE 15 #12 - ESQUINA', style: 'font-size: 10px; margin: 0px 6px;' },
        { type: 'text', value: 'SERVICIO LAS 24 HORAS DEL DIA', style: 'font-size: 10px; margin: 0px 6px;' },
        { type: 'text', value: '===========================', style: 'font-size: 12px; margin: 0px 6px;' },
        { type: 'text', value: 'AUTOMOTOR: '+ record.automotor, style: 'font-size: 12px; margin: 0px 6px;' },
    ];

    if (automotor.enabled_items == true) {
        data.push({ type: 'text', value: automotor.automotor_items +': '+ record.cant_automotor_items, style: 'font-size: 12px; margin: 0px 6px;' });
    }

    data.push(
        { type: 'text', value: 'VALOR TARIFA: '+ rate.value_rate +' por '+ name, style: 'font-size: 12px; margin: 0px 6px;' },
        { type: 'text', value: 'VALOR TOTAL: '+ addPoints(paymentAmountClean.toString()).split(',')[0], style: 'font-size: 12px; margin: 0px 6px;' },
        { type: 'text', value: 'EFECTIVO: '+ addPoints(paymentReceivedClean.toString()).split(',')[0], style: 'font-size: 12px; margin: 0px 6px;' },
        { type: 'text', value: 'CAMBIO: '+ exchange, style: 'font-size: 12px; margin: 0px 6px;' },
        { type: 'text', value: '', style: 'padding: 5% 0%; margin: 0px 6px;' },
        { type: 'text', value: record.plate, style: 'font-size: 34px; font-weight: bold; text-align: center; margin: 0px 6px;' },
        { type: 'text', value: '===========================', style: 'font-size: 12px; margin: 0px 6px;' },
        { type: 'text', value: 'FECHA Y HORA DE INGRESO: ', style: 'font-size: 12px; text-align: center; margin: 0px 6px;' },
        { type: 'text', value: newStartDate, style: 'font-size: 12px; text-align: center; margin: 0px 6px;' },
        { type: 'text', value: 'FECHA Y HORA DE SALIDA: ', style: 'font-size: 12px; text-align: center; margin: 0px 6px;' },
        { type: 'text', value: newEndDate, style: 'font-size: 12px; text-align: center; margin: 0px 6px;' },
        { type: 'text', value: '', style: 'padding: 5% 0%; margin: 0px 6px;' },
        { type: 'text', value: ticketDescription, style: 'font-size: 10px; text-align: justify; font-style: italic; margin: 0px 6px;' },
    );

    printer(data);
}

function printer(data){
    const printers = win.webContents.getPrinters();
    let defaultPrinterName = '';

    for (let i = 0; i < printers.length; i++) {
        if (printers[i].isDefault == true) {
            defaultPrinterName = printers[i].name;
        }   
    }
    
    PosPrinter.print(data, {
        silent: true,
        preview: false,
        width: '100%',
        margin: '0 0 0 0',
        copies: 1,
        printerName: defaultPrinterName,
        timeOutPerLine: 10000,
        pageSize: { height: 301000, width: 71000 }
    }).then((success) => {
        console.log('ajam'); 
        console.log(success); 
    }).catch((error) => { 
        console.error('ujum'); 
        console.error(error); 
    });
}

function compareTwoDates(prevTime, currentDate){
    // asignar el valor de las unidades en milisegundos
    var msecPerMinute = 1000 * 60;
    var msecPerHour = msecPerMinute * 60;
    var msecPerDay = msecPerHour * 24;

    // Obtener la diferencia en milisegundos
    var interval = currentDate.getTime() - prevTime.getTime();

    // Calcular cuentos días contiene el intervalo. Substraer cuantos días
    //tiene el intervalo para determinar el sobrante
    var days = Math.floor(interval / msecPerDay );
    // interval = interval - (days * msecPerDay );

    // Calcular las horas , minutos y segundos
    var hours = Math.floor(interval / msecPerHour );
    // interval = interval - (hours * msecPerHour );

    var minutes = Math.floor(interval / msecPerMinute );
    // interval = interval - (minutes * msecPerMinute );

    var seconds = Math.floor(interval / 1000 );

    // Mostrar el resultado.
    return {'days': days, 'hours': hours, 'minutes': minutes, 'seconds': seconds};
}

function getCurrentDate(){
    let currentDateTime = new Date();
    let arrayTime = currentDateTime.toLocaleTimeString('en-US', { hour12: false }).toString().split(':');

    return currentDateTime = new Date(Date.UTC(currentDateTime.getFullYear(), currentDateTime.getMonth(), currentDateTime.getDate(), arrayTime[0], arrayTime[1], arrayTime[2], currentDateTime.getMilliseconds()));
}

function addPoints(nStr) {
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;

    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    
    var result = x1 + x2;
    return result + ',00';
}

function parseDateDayMonthYear(date) { 
    let newDate = date.toISOString().split('T')[0];
    newDate = newDate.substring(8,10) +'/'+ newDate.substring(5,7) +'/'+ newDate.substring(0,4);
    
    return newDate;
}

function formatAMPM(date) {
    var hours = date.getUTCHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}