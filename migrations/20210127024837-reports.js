'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Reports', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.INTEGER,
        autoIncrement: true
      },
      client: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: 'Clients',
          key: 'id'
        }
      },
      automotor: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Automotores',
          key: 'id'
        }
      },
      plate: {
          type: Sequelize.STRING,
          allowNull: false
      },
      rate: {
          type: Sequelize.STRING,
          allowNull: false,
      },
      report_start_date: {
          type: Sequelize.DATE,
          allowNull: false
      },
      report_end_date: {
          type: Sequelize.DATE,
          allowNull: false
      },
      status: {
          type: Sequelize.BOOLEAN,
          allowNull: false
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Reports');
  }
};
