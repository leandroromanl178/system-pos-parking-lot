'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Rates', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.INTEGER,
        autoIncrement: true
      },
      automotor_type: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: 'Automotores',
          key: 'id'
        }
      },
      type_rate: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      value_rate: {
        type: Sequelize.STRING,
        allowNull: false
      }
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Rates');
  }
};
