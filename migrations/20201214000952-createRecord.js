'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Records', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4
      },
      user_employed: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
          model: 'Users',
          key: 'user_id'
        }
      },
      automotor: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
            model: 'Automotores',
            key: 'id'
        }
      },
      cant_automotor_items: {
        type: Sequelize.INTEGER,
        allowNull: true
      },
      rate: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
            model: 'Rates',
            key: 'id'
        }
      },
      plate: {
        type: Sequelize.STRING,
        allowNull: false
      },
      start_date_time: {
        type: Sequelize.DATE,
        allowNull: false
      },
      end_date_time: {
        type: Sequelize.DATE,
        allowNull: true
      }
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Records');
  }
};
