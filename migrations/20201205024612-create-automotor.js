'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Automotores', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.INTEGER,
        autoIncrement: true
      },
      automotor_type: {
        type: Sequelize.STRING,
        allowNull: false
      },
      automotor_items: {
        type: Sequelize.STRING,
        allowNull: true
      },
      enabled_items: {
        type: Sequelize.BOOLEAN,
        allowNull:false
      }
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Automotores');
  }
};
