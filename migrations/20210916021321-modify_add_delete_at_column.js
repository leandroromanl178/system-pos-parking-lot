'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        'Records', // table name
        'delete_at', // new field name
        {
          type: Sequelize.DATE,
          allowNull: true,
        },
      ),
      queryInterface.addColumn(
        'Reports',
        'delete_at',
        {
          type: Sequelize.DATE,
          allowNull: true,
        },
      )
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('Reports', 'delete_at'),
      queryInterface.removeColumn('Records', 'delete_at')
    ]);
  }
};
