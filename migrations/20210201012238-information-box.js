'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('Information', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.INTEGER,
        autoIncrement: true
      },
      user: {
        type: Sequelize.UUID,
        allowNull: false,
        references: {
            model: 'Users',
            key: 'user_id'
        }
      },
      amount: {
        type: Sequelize.STRING,
        allowNull: false
      },
      regist_date: {
        type: Sequelize.DATE,
        allowNull: true
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Information');
  }
};
