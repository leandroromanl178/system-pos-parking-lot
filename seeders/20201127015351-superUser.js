'use strict';

const bcrypt = require('bcrypt')
const { v4: uuidv4 } = require('uuid')
const passwordEncode = bcrypt.hashSync('123', 10);

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Users', [{
      user_id: uuidv4(),
      name: 'LRoman',
      lastName: 'Soy Admin',
      username: 'admin',
      rol: 'SuperUser',
      password: passwordEncode,
      isAdmin: true,
      createdAt: new Date(),
      updatedAt: new Date()
    }]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Users', null, {});
  }
};
