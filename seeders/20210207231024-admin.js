'use strict';

const bcrypt = require('bcrypt')
const { v4: uuidv4 } = require('uuid')
const passwordEncode = bcrypt.hashSync('321', 10);

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Users', [{
      user_id: uuidv4(),
      name: 'Alberto',
      lastName: 'Castillo',
      username: 'Administrador',
      rol: 'Administrador',
      password: passwordEncode,
      isAdmin: true,
      createdAt: new Date(),
      updatedAt: new Date()
    }]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Users', null, {});
  }
};
